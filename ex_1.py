# # # # # Write a program which will find all such numbers which are divisible by 7 but are not a multiple of 5,
# # # # # between 2000 and 3200 (both included).
# # # # # The numbers obtained should be printed in a comma-separated sequence on a single line.
# # # # #
# # # # # Hints:
# # # # # Consider use range(#begin, #end) method
# # # #
# # # # l=[]
# # # # for i in range(2000, 3201):
# # # #     if (i%7==0) and (i%5!=0):
# # # #         l.append(str(i))
# # # # print(l)
# # # #
# # # # Question:
# # # # Write a program which can compute the factorial of a given numbers.
# # # # The results should be printed in a comma-separated sequence on a single line.
# # # # Suppose the following input is supplied to the program:
# # # # 8
# # # # Then, the output should be:
# # # # 40320
# # # #
# # # # Hints:
# # # # In case of input data being supplied to the question, it should be assumed to be a console input.
# # #
# # # def fact(x):
# # #     if x == 0:
# # #         return 1
# # #     else:
# # #         return x * fact(x-1)
# # # x=int(input('intru un nr'))
# # #
# # # print (fact(x))
# # #
# # # With a given integral number n, write a program to generate a dictionary that contains (i, i*i) such that
# # # is an integral number between 1 and n (both included). and then the program should print the dictionary.
# # # Suppose the following input is supplied to the program:
# # # 8
# # # Then, the output should be:
# # # {1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36, 7: 49, 8: 64}
# # #
# # # Hints:
# # # In case of input data being supplied to the question, it should be assumed to be a console input.
# # # Consider use dict()
# #
# # n=int(input('intru un nr'))
# # d=dict()
# # for i in range(1,n+1):
# #     d[i]=i*i
# # print(d)
# #
# # Write a program which accepts a sequence of comma-separated numbers from console and generate a list and a tuple
# # which contains every number.
# # Suppose the following input is supplied to the program:
# # 34,67,55,33,12,98
# # Then, the output should be:
# # ['34', '67', '55', '33', '12', '98']
# # ('34', '67', '55', '33', '12', '98')
# #
# # Hints:
# # In case of input data being supplied to the question, it should be assumed to be a console input.
# # tuple() method can convert list to tuple
#
# # valori=input('introduceti nr')
# # l=valori.split(",")
# # t=tuple(l)
# # print(valori)
# # print(t)
#
#

import random

# import random
#
# random_number = random.randint(1, 10)
# guess_left = 3
# while guess_left > 0:
#     random_number = int(input('Guess a number between 1 and 10: '))
#     if random_number == random:
#         print('you win')
#         break
#     guess_left -= 1
# else:
#     print('you lose')

# SCORE = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2,
#           "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3,
#           "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1,
#           "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4,
#           "x": 8, "z": 10}
# # #
# def scrabble_score(word):
#     score = 0
#     for letter in word:
#         score += SCORE[letter.lower()]
# #     total = 0
# #     word = input('text:')
# #     for letter in word:
# #         total += SCORE[letter]
#     return score
# #
# print(scrabble_score('saluc'))

# def hotel_cost(nights):
#   return 140 * nights
#
# def plane_ride_cost(city):
#   if city == "Charlotte":
#     return 183
#   elif city == "Tampa":
#     return 220
#   elif city == "Pittsburgh":
#     return 222
#   elif city == "Los Angeles":
#     return 475
#   else:
#     return "Invalid city. Please enter a valid destination."
# print(hotel_cost(5))
# print(plane_ride_cost("Tampa"))
#
# def rental_car_cost(days):
#   cost = 40 * days
#   if days >= 7:
#     cost -= 50
#   elif days >= 3:
#     cost -= 20
#   return cost
#
# def trip_cost(city, days, spending_money):
#   return rental_car_cost(days) + hotel_cost(days) + plane_ride_cost(city) + spending_money
# print(trip_cost("Tampa", 5, 600))  # Returns the cost of a 5-day trip to Tampa with $600 spending money

# Get student's tutorial and test marks
# tutorial_mark = float(input("Enter the student's tutorial mark: "))
# test_mark = float(input("Enter the student's test mark: "))
#
# # Calculate the average of the tutorial and test marks
# average = (tutorial_mark + test_mark) / 2
#
# # If the average is lower than 40%, the student gets an F grade
# if average < 40:
#   print("F")
#   exit()
#
# # Get the student's examination mark
# examination_mark = float(input("Enter the student's examination mark: "))
#
# # Calculate the student's final mark
# final_mark = (tutorial_mark * 0.25) + (test_mark * 0.25) + (examination_mark * 0.5)
#
# # Calculate the student's grade
# if final_mark >= 80:
#   grade = "A"
# elif final_mark >= 70:
#   grade = "B"
# elif final_mark >= 60:
#   grade = "C"
# elif final_mark >= 50:
#   grade = "D"
# else:
#   grade = "E"
#
# # Print the grade
# print(grade)


# class Carte():
#     def __init__(self, titlu, autor):
#         self.titlu = titlu
#         self.autor = autor
#
# carte1 = Carte("morometii", "Marin Preda")
# carte2 = Carte("Harry Potter", "Rowling")

# a = int(input("introdu un nr: "))
# b = int(input("introdu alt nr: "))
# operator = input("operator = " )
#
# if operator == "+":
#     print(a + b)
# elif operator == "-":
#     print(a - b)
# elif operator == "*":
#     print(a * b)
# elif operator == "/":
#     if b == 0:
#         print("eroare")
#     else:
#         print(a / b)
# else:
#     print("cunosc doar 4 operatii")


# d = {'a': 1, 'b': 2, 'c': 3}
# for key, value in d.items():
#     print(key, value)
#
# d = {'a': 1, 'b': 2, 'c': 3}
# for key in d.keys():
#     print(key)
#
# lst = [1, 2, 3, 4, 5]
# for item in lst:
#     print(item)
#
# lst = [1, 2, 3, 4, 5]
# for index, item in enumerate(lst):
#     print(index, item)

# def my_decorator(func):
#     def wrapper():
#         print("Something is happening before the function is called.")
#         func()
#         print("Something is happening after the function is called.")
#     return wrapper
#
# @my_decorator
# def new_function():
#     print("bau!")
#
# new_function()


# def salut(nume):
#     print("Salut, " + nume + " Ce mai faci?")
#
# salut("George")


import random

# Stocati intrebarile si optiunile de raspuns intr-un dicționar
questions = [{'Intrebare': 'Care este capitala Frantei?',
              'Optiuni': ['Paris', 'Lyon', 'Marseille'],
              'Raspuns corect': 'Paris'},
             {'Intrebare': 'Care este cel mai mare ocean din lume?',
              'Optiuni': ['Pacific', 'Atlantic', 'Indian'],
              'Raspuns corect': 'Pacific'}]

# Parcurgeti fiecare intrebare si amestecati variantele de raspuns
for question in questions:
    random.shuffle(question['Optiuni'])

# Afisati intrebarile si variantele de raspuns amestecate
for question in questions:
    print(question['Intrebare'])
    for i, option in enumerate(question['Optiuni']):
        print(f"{i + 1}. {option}")
    print("")


