import yfinance as yf
import matplotlib.pyplot as plt

ticker = "AAPL"
start_date = "2022-01-01"
end_date = "2022-12-31"

df = yf.download(ticker, start=start_date, end=end_date)

df['High'].plot(label='High', figsize=(16,8), title=ticker)
df['Low'].plot(label='Low')

plt.xlabel('Date')
plt.ylabel('Price')
plt.legend()

# Calculate the rolling mean to use as a baseline for the support and resistance lines
rolling_mean = df['Close'].rolling(window=20).mean()

# Plot the rolling mean
rolling_mean.plot(label='Rolling Mean', figsize=(16,8), title=ticker)

# Plot the support line as the rolling mean minus 2 times the rolling standard deviation
rolling_std = df['Close'].rolling(window=20).std()
support_line = rolling_mean - 2 * rolling_std
support_line.plot(label='Support Line', figsize=(16,8), title=ticker, linestyle='--')

# Plot the resistance line as the rolling mean plus 2 times the rolling standard deviation
resistance_line = rolling_mean + 2 * rolling_std
resistance_line.plot(label='Resistance Line', figsize=(16,8), title=ticker, linestyle='--')

plt.xlabel('Date')
plt.ylabel('Price')
plt.legend()
plt.show()


